Marathi MTLD Tool ![v0.1](https://img.shields.io/badge/Version-0.1-success) ![Language: Python](https://img.shields.io/badge/Language-Python-yellow)
---
The Marathi MTLD Tool is developed for an academic purpose. It is a part of M. Phil. research work submitted to the Department of Linguistics, University of Mumbai in 2021.
# Description

This tool calculates the number of repeated words present in the Marathi text written in the Devnagari script by using the Measure of Textual Lexical Diversity (MTLD) approach. For further details of the MTLD approach refer to McCarthy (2005)[^fn]

[^fn]: McCarthy, P. M. (2005). An assessment of the range and usefulness of lexical diversity measures and the potential of the measure of textual, lexical diversity (mtld) (Doctoral dissertation, The University of Memphis)

Before calculating the MTLD score of an input text, the Marathi MTLD program also performs the lemmatization process. The lemmatization process is carried out in two steps:

1. Lemmatization stage 1: In this step, the complex words present in an input text undergo the Stemming process. To perform stemming operation, two dependencies **indic_nlp_resource** and **indic_nlp_library** are used. The non-words generated in the Stemming process are discarded with the support of **Exception list resource**.

2. Lemmatization stage 2: This step provides lemma forms to all those words which are not converted into their lemma forms in the Stemming process. Marathi MTLD tool performs this operation by making use of **Lemma form library**.

The texts which satisfy two conditions: one, the text should be written in the Devanagari script and two, the text should be the text of Marathi language, can be inserted as an input text. The Input section will accept input text in three different ways i.e. copy-pasting, typing and uploading. Users can access the output of the Marathi MTLD tool on the program window itself. In addition, the Marathi MTLD program also provides an option to save the output data. Users can save the output in three different formats i.e. CSV, EXCEL and ODS. For more details refer to _Manual of Marathi MTLD Tool_.
 
# Installation instrctutions
## For GNU/Linux:

1. Download the python from the PYTHON website   (https://www.python.org/) and install it.

2. Install pip  
   
   ```
   sudo apt-get install pip 
   ```

3. Install PyQt5 designer tool 
   
   ``` 
   sudo pip install PyQt5 
   ```

4. Install morfessor 
   
   ```
   sudo pip install morfessor 
   ```

5. Install pandas 
   
   ```
   sudo pip install pandas 
   ```

6. Install docx2txt 
   
   ```
   sudo pip install docx2txt 
   ```

7. Install pyexcel_ods 
   
   ```
   sudo pip install pyexcel_ods 
   ```

8. Install openpyxl 
   
   ```
   sudo pip install openpyxl 
   ```

9. Download source code of Marathi MTLD tool project from https://gitlab.
com/priyankaD3/mr-mtld

10. Run the program 
   
    ```
    python3 marathi-mtld-tool.py 
    ```

## For Windows

1. Download the python from the PYTHON website   (https://www.python.org/) and install it.

2. Install PyQt5 designer tool  

   ```   
   pip install PyQt5 
   ```

3. Install morfessor 
   
   ```   
   pip install morfessor 
   ```

4. Install pandas 

   ``` 
   pip install pandas
   ```

5. Install docx2txt

   ``` 
   pip install docx2txt 
   ```

6. Install pyexcel_ods 
   
   ``` 
   pip install pyexcel-ods 
   ```

7. Install openpyxl 
   
   ``` 
   pip install openpyxl 
   ```

8. Download source code of Marathi MTLD tool project from https://gitlab.
com/priyankaD3/mr-mtld

9. Run the program  

   ```
   py marathi-mtld-tool.py 
   
   ```

The executable file of the Marathi MTLD tool is available at the following links:

For GNU/Linux: https://sourceforge.net/projects/mr-mtld-linux/

For Windows: https://sourceforge.net/projects/mr-mtld-windows/ 

# Acknowledgement
   
# License

© 2021 Priyanka Dingankar; Suhas Zimbar; Department of Linguistics, University of Mumbai

## Runtime code

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see:

<https://www.gnu.org/licenses/>.

## Documentation 

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section entitled "GNU Free Documentation License".

#### Footnotes
